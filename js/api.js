const host = window.location.href.includes("netlify") ? "https://book-store-jafar.herokuapp.com/" : "http://127.0.0.1:8000/"

const registerApi = (email, password) => {
    return new Promise((res, rej) => {
        const formdata = new FormData();
        formdata.append("email", email);
        formdata.append("password", password);

        const requestOptions = {
            method: 'POST',
            body: formdata,
            redirect: 'follow'
        };

        fetch(host + "auth/register/", requestOptions)
            .then(async response => {
                if (response.status === 201) {
                  const data = await response.json()
                  res(data);
                } else if (response.status === 400){
                  rej({msg: "data input tidak valid"})
                }else{
                  rej({msg: "Terjadi kesalahan server"})
                }
            }).catch(error => {
            rej({msg: "Connection error"})
        });
    })
}

const loginApi = (email, password) => {
    return new Promise((res, rej) => {
        const formdata = new FormData();
        formdata.append("email", email);
        formdata.append("password", password);

        const requestOptions = {
            method: 'POST',
            body: formdata,
            redirect: 'follow'
        };

        fetch(host + "auth/login/", requestOptions)
            .then(async response => {
                if (response.status === 200) {
                    const data = await response.json()
                    res(data.access);
                }else if (response.status >= 500) {
                  rej({msg: "Server error"})
                } else {
                  rej({msg: "User not found"})
                }
            }).catch(error => {
              rej({"message": "Connection error"})
            });
    })
}
