let logged_in = false;
let token = null;
let email = null
let role = null;
let user_id = null;
var authNav= document.getElementById('auth-nav')
var noAuthNav = document.getElementById("no-auth-nav")

$(()=>{
    const m_token = localStorage.getItem("token");
    if (m_token == null) {
      logged_in = false;
    } else {
      try {
        const decoded_token = window.jwt_decode(m_token);
        email = decoded_token.email;
        role = decoded_token.role;
        user_id = decoded_token.user_id;
        logged_in = true;
        token = m_token;
      } catch (e) {
        logged_in=false;
      }
    }
    try {
      if (logged_in) {
        authNav.classList.remove("hidden");
      } else {
        noAuthNav.classList.remove("hidden")
      }
    } catch (e) {}
})

const check_logged_in = () => {
    return logged_in;
}

const force_log_in = () => {
    if (!check_logged_in()) {
        location.href = "./login.html?next=" + window.location;
    }
}

const force_admin = () => {
    if (!check_admin()) {
        localStorage.setItem("authError", "Anda tidak diperkenankan melihat halaman tersebut");
        history.back();
    }
}


const check_admin= () => {
    if (!logged_in || role !== "ADMIN") {
        return false;
    } else {
        return true;
    }
}

const force_anggota = () => {
    if (!check_anggota()) {
        localStorage.setItem("authError", "Anda tidak diperkenankan melihat halaman tersebut");
        history.back();
    }
}

const logout = () => {
    logged_in = false;
    role = null;
    email = null;
    user_id = null;
    token = null;
    localStorage.removeItem("token")
    location.href = "../index.html"
}

const check_anggota = () => {
    if (!logged_in || role !== "ANGGOTA") {
        return false;
    } else {
        return true;
    }
}


const get_token = () => {
    return token;
}

const register = (email, password) => {
    registerApi(email, password).then( data => {
      Swal.fire({
        title: 'Berhasil',
        text: "Berhasil membuat akun",
        icon: 'success',
      }).then((result) => {
        if (result.isConfirmed) {
          location.href = "./login.html";
        }
      })
    }).catch( error => {
        Swal.fire("Error", error.msg, 'error');
    })
}

const login = (email, password) => {
    const urlSearchParams = new URLSearchParams(window.location.search);
    loginApi(email, password).then(token => {
        logged_in = true;
        localStorage.setItem("token", token)
        localStorage.getItem("token")
        if (urlSearchParams.has("next")) {
            location.href = urlSearchParams.get("next")
        }else{
            location.href = "./dashboard.html";
        }
    }).catch( error => {
        Swal.fire(error.msg, '', 'error');
    })
}
