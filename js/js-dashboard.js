let tabsContainer = document.querySelector("#tabs");
let tabTogglers = tabsContainer.querySelectorAll("a");
let tabsContainerAdmin = document.querySelector("#tabs-admin");
let tabTogglersAdmin = tabsContainerAdmin.querySelectorAll("a");

const colorStatusOrder = {
  "Rejected": "bg-red-600",
  "Waiting for Payment": "bg-zinc-500",
  "Processing": "bg-teal-600",
  "In Delivery": "bg-teal-500",
  "Delivered": "bg-teal-400",
  "Finished": "bg-teal-300"
}

const statusOrder = {
  "Rejected": "Gagal",
  "Waiting for Payment": "Menunggu Pembayaran",
  "Processing": "Diproses",
  "In Delivery": "Dalam Pengiriman",
  "Delivered": "Terkirim",
  "Finished": "Selesai"
}

const getAllOrderByCustomer = () => {
  return new Promise((res, rej) => {
    const requestOptions = {
      method: 'GET',
      headers: {"Authorization": "Bearer " + token}
    };
    fetch(host + "order/me/", requestOptions)
      .then(async response => {
        if (response.status === 200) {
          const data = await response.json()
          res(data);
        } else {
          rej({msg: "Server error"})
        }
      }).catch(error => {
      rej({msg: "Connection error"})
    });
  })
}

const getAllOrderByAdmin = () => {
  return new Promise((res, rej) => {
    const requestOptions = {
      method: 'GET',
      headers: {"Authorization": "Bearer " + token}
    };
    fetch(host + "order/admin/", requestOptions)
      .then(async response => {
        if (response.status === 200) {
          const data = await response.json()
          res(data);
        } else {
          rej({msg: "Server error"})
        }
      }).catch(error => {
      rej({msg: "Connection error"})
    });
  })
}

$(()=> {
  force_log_in()
  if(role === 'CUSTOMER'){
    document.getElementById("customer").classList.remove("hidden")
    toggleCustomer()
    document.getElementById("default-tab").click();
    getAllOrderByCustomer().then(data =>{
      for(let i=0; i<data.length; i++){
        let buttonFinish = ""
        if(data[i].order_status !== "Delivered"){
          buttonFinish = "hidden"
        }
        if (data[i].order_status === "Finished"){
          cardOrder = `<div class="relative z-0 mx-2 my-4 card w-[400px] p-4 bg-gray-300 rounded-xl drop-shadow-lg border-none">
          <p class="font-bold w-full truncate">${data[i].judul}</p>
          <p class="truncate">${data[i].penulis}</p>
          <p class="truncate">${data[i].penerbit} </p>
          <div class="flex justify-between mt-2">
            <p class="truncate w-1/2 block"><span class="font-semibold">Jumlah Barang</span><br>${data[i].quantity}</p>
            <p class="truncate w-1/2 block"><span class="font-semibold">Total harga</span><br>Rp ${data[i].total_harga}</p>
          </div>
          <div class="mt-2">
            <span class="font-semibold">Status Order</span><br>
            <div class="flex justify-between">
                <div class="font-bold p-2 ${colorStatusOrder[data[i].order_status]} rounded-md inline-block">
                    ${statusOrder[data[i].order_status]}
                </div>
                <div class="${buttonFinish}">
                    <button onclick="finishButtonClicked(${data[i].id})" class="text-white font-bold bg-zinc-700 hover:bg-zinc-600 ml-3 p-2 rounded-md">
                        Selesaikan Order
                    </button>
                </div>
            </div>
          </div>
        </div>`
          $('#second').append(cardOrder)
        }
        else{
          cardOrder = `<div class="relative z-0 mx-2 my-4 card w-[400px] p-4 bg-gray-300 rounded-xl drop-shadow-lg border-none">
           <div class="hidden">$data[i].id</div>
          <p class="font-bold w-full truncate">${data[i].judul}</p>
          <p class="truncate">${data[i].penulis}</p>
          <p class="truncate">${data[i].penerbit} </p>
          <div class="flex justify-between mt-2">
            <p class="truncate w-1/2 block"><span class="font-semibold">Jumlah Barang</span><br>${data[i].quantity}</p>
            <p class="truncate w-1/2 block"><span class="font-semibold">Total harga</span><br>Rp ${data[i].total_harga}</p>
          </div>
          <div class="mt-2">
            <span class="font-semibold">Status Order</span><br>
            <div class="flex justify-between">
                <div class="font-bold p-2 ${colorStatusOrder[data[i].order_status]} rounded-md inline-block">
                    ${statusOrder[data[i].order_status]}
                </div>
                <div class="${buttonFinish}">
                    <button onclick="finishButtonClicked(${data[i].id})" class="text-white font-bold bg-zinc-700 hover:bg-zinc-600 ml-3 p-2 rounded-md">
                        Selesaikan Order
                    </button>
                </div>
            </div>
          </div>
        </div>`
          $('#first').append(cardOrder)
        }
      }
    }).catch( error => {
    })
  }else{
    document.getElementById("admin").classList.remove("hidden")
    toggleAdmin()
    document.getElementById("default-tab-admin").click();
    getAllOrderByAdmin().then(data =>{
      for (let i = 0; i < data.length; i++){
        let buttonUpdate = "hidden"
        if(data[i].order_status !== "Delivered"){
          buttonUpdate = ""
        }
        if(data[i].order_status === "Finished"){
          cardOrder = `<div class="relative z-0 mx-2 my-4 card w-[400px] p-4 bg-gray-300 rounded-xl drop-shadow-lg border-none">
          <p class="font-bold w-full truncate">${data[i].judul}</p>
          <p class="truncate">${data[i].penulis}</p>
          <p class="truncate">${data[i].penerbit} </p>
          <div class="flex justify-between mt-2">
            <p class="truncate w-1/2 block"><span class="font-semibold">Jumlah Barang</span><br>${data[i].quantity}</p>
            <p class="truncate w-1/2 block"><span class="font-semibold">Total harga</span><br>Rp ${data[i].total_harga}</p>
          </div>
          <div class="mt-2">
            <span class="font-semibold">Status Order</span><br>
            <div class="flex justify-between">
                <div class="font-bold p-2 ${colorStatusOrder[data[i].order_status]} rounded-md inline-block">
                    ${statusOrder[data[i].order_status]}
                </div>
                <div class="hidden">
                    <button onclick="updateStatusPemesanan(${data[i].id})" class="text-white font-bold bg-zinc-700 hover:bg-zinc-600 ml-3 p-2 rounded-md">
                        Update Status
                    </button>
                </div>
            </div>
          </div>
        </div>`
          $('#second-admin').append(cardOrder)
        }else{
          cardOrder = `<div class="relative z-0 mx-2 my-4 card w-[400px] p-4 bg-gray-300 rounded-xl drop-shadow-lg border-none">
          <p class="font-bold w-full truncate">${data[i].judul}</p>
          <p class="truncate">${data[i].penulis}</p>
          <p class="truncate">${data[i].penerbit} </p>
          <div class="flex justify-between mt-2">
            <p class="truncate w-1/2 block"><span class="font-semibold">Jumlah Barang</span><br>${data[i].quantity}</p>
            <p class="truncate w-1/2 block"><span class="font-semibold">Total harga</span><br>Rp ${data[i].total_harga}</p>
          </div>
          <div class="mt-2">
            <span class="font-semibold">Status Order</span><br>
            <div class="flex justify-between">
                <div class="font-bold p-2 ${colorStatusOrder[data[i].order_status]} rounded-md inline-block">
                    ${statusOrder[data[i].order_status]}
                </div>
                <div class="${buttonUpdate}">
                    <button onclick="updateStatusPemesanan(${data[i].id})" class="text-white font-bold bg-zinc-700 hover:bg-zinc-600 ml-3 p-2 rounded-md">
                        Update Status
                    </button>
                </div>
            </div>
          </div>
        </div>`
          $('#first-admin').append(cardOrder)
        }
      }
    })
  }
});

const finishOrderAPI= (idBook) => {
  return new Promise((res, rej) => {
    const requestOptions = {
      method: 'POST',
      headers: {"Authorization": "Bearer " + token}
    };

    fetch(host + `order/finish/${idBook}/`, requestOptions)
      .then(async response => {
        if (response.status === 201) {
          const data = await response.json()
          res(data);
        }else if (response.status == 403){
          rej({msg: "Anda tidak memiliki akses"})
        }else if (response.status == 400){
          rej({msg: "Terjadi kesalahan input"})
        }else if (response.status == 404){
          rej({msg: "Order tidak ditemukan"})
        }else{
          rej({msg: "Terjadi kesalahan server"})
        }
      }).catch(error => {
      rej({msg: "Connection error"})
    });
  })
}


const finishButtonClicked = (idBook)=>{
  finishOrderAPI(idBook).then(data =>{
    Swal.fire({
      title: 'Berhasil',
      text: "Berhasil menyelesaikan order",
      icon: 'success',
    }).then((result) => {
      if (result.isConfirmed) {
        location.reload()
      }
    })
  }).catch(error =>{
    Swal.fire("Terjadi kesalahan", error.msg, "error")
  })
}


const updateStatusAPI= (id) => {
  return new Promise((res, rej) => {
    const requestOptions = {
      method: 'POST',
      headers: {"Authorization": "Bearer " + token}
    };

    fetch(host + `order/admin/update_order/${id}/`, requestOptions)
      .then(async response => {
        if (response.status === 201) {
          const data = await response.json()
          res(data);
        }else if (response.status == 403){
          rej({msg: "Anda tidak memiliki akses"})
        }else if (response.status == 400){
          rej({msg: "Terjadi kesalahan input"})
        }else if (response.status == 404){
          rej({msg: "Order tidak ditemukan"})
        }else{
          rej({msg: "Terjadi kesalahan server"})
        }
      }).catch(error => {
      rej({msg: "Connection error"})
    });
  })
}


const updateStatusPemesanan = (id)=>{
  updateStatusAPI(id).then(data =>{
    Swal.fire({
      title: 'Berhasil',
      text: "Berhasil mengubah status order",
      icon: 'success',
    }).then((result) => {
      if (result.isConfirmed) {
        location.reload()
      }
    })
  }).catch(error =>{
    Swal.fire("Terjadi kesalahan", error.msg, "error")
  })
}

const toggleCustomer = ()=>{
  tabTogglers.forEach(function(toggler) {
    toggler.addEventListener("click", function(e) {
      e.preventDefault();

      let tabName = this.getAttribute("href");

      let tabContents = document.querySelector("#tab-contents");

      for (let i = 0; i < tabContents.children.length; i++) {

        tabTogglers[i].parentElement.classList.remove("border-black", "border-b-4",  "-mb-px", "opacity-100");  tabContents.children[i].classList.remove("hidden");
        if ("#" + tabContents.children[i].id === tabName) {
          continue;
        }
        tabContents.children[i].classList.add("hidden");

      }
      e.target.parentElement.classList.add("border-black", "border-b-4", "-mb-px", "opacity-100");
    });
  });
}

const toggleAdmin = ()=>{
  tabTogglersAdmin.forEach(function(toggler) {
    toggler.addEventListener("click", function(e) {
      e.preventDefault();

      let tabName = this.getAttribute("href");
      let tabContents = document.querySelector("#tab-contents-admin");

      for (let i = 0; i < tabContents.children.length; i++) {
        tabTogglersAdmin[i].parentElement.classList.remove("border-black", "border-b-4",  "-mb-px", "opacity-100");  tabContents.children[i].classList.remove("hidden");
        if ("#" + tabContents.children[i].id === tabName) {
          continue;
        }
        tabContents.children[i].classList.add("hidden");

      }
      e.target.parentElement.classList.add("border-black", "border-b-4", "-mb-px", "opacity-100");
    });
  });
}
