const getDetailBuku = (id) => {
  return new Promise((res, rej) => {
    const requestOptions = {
      method: 'GET'
    };
    fetch(host + `book/${id}/`, requestOptions)
      .then(async response => {
        if (response.status === 200) {
          const data = await response.json()
          res(data);
        } else if (response.status === 404) {
          rej({msg: "Buku tidak ditemukan"})
        } else {
          rej({msg: "Server error"})
        }
      }).catch(error => {
      rej({msg: "Connection error"})
    });
  })
}
const urlSearch = window.location.search
const urlParams = new URLSearchParams(urlSearch)
const idBook = urlParams.get("id")

$(() => {
  getDetailBuku(idBook).then(data => {
    let btnBeli = "hidden"
    let btnEdit = "hidden"
    if (role === 'ADMIN'){
      btnEdit = ""
    }else if (role === "CUSTOMER"){
      btnBeli = ""
    }
    let detailBook = `<div class="mb-8">
        <p class="font-bold text-2xl">${data.title}</p>
        <p class="font-semibold">${data.author}</p>
        </div>`
    detailBook += `<div class="h-1/3 mb-8">
    <span class="font-bold text-lg">Sinopsis</span>
    <p>
    ${data.synopsis}
    </p>
  </div>`
    detailBook += `<div class="">
    <span class="font-bold text-lg">Detail Buku</span>
    <div class="flex justify-between">
      <span class="w-1/2">
        <p class="mt-2"><span class="font-semibold">Penerbit</span> <br> ${data.publisher}</p>
        <p class="mt-2"><span class="font-semibold">Tanggal Publikasi</span><br> ${data.date_published}</p>
        <p class="mt-2"><span class="font-semibold mt-2">Jumlah Halaman</span><br> ${data.pages}</p>
        <p class="mt-2"><span class="font-semibold mt-2">Kategori</span><br> ${data.category}</p>
      </span>
      <span class="w-1/2">
        <p class="mt-2"><span class="font-semibold">ISBN</span><br>${data.isbn}</p>
        <p class="mt-2"><span class="font-semibold">Stok</span><br> ${data.stocks}</p>
        <div class="mt-2 font-semibold">
          <span class="">Harga</span><br>
          <div class="underline text-lg text-green-700">Rp ${data.prices}</div>
        </div>
        <div class="flex ${btnBeli} items-center mt-2">
            <div class="flex w-[100px] flex-row h-10 w-full rounded-lg relative bg-transparent">
              <button data-action="decrement" id="decrement-button" class=" bg-gray-400  text-gray-600 hover:text-gray-700 hover:bg-gray-500 h-full w-20 rounded-l cursor-pointer outline-none">
                <span class="m-auto text-2xl font-thin">−</span>
              </button>
            <input type="number" class="outline-none focus:outline-none text-center w-full bg-gray-400 font-semibold text-md hover:text-black focus:text-black  md:text-basecursor-default flex items-center text-gray-700  outline-none" id="input-quantity" name="custom-input-number" value="0"></input>
            <button data-action="increment" class="bg-zinc-400 text-gray-600 hover:text-gray-700 hover:bg-gray-500 h-full w-20 rounded-r cursor-pointer">
                <span class="m-auto text-2xl font-thin">+</span>
            </button>
        </div>
          <button onclick="createOrder()" class="text-white text-lg font-bold bg-zinc-700 hover:bg-zinc-600 w-[100px] h-10 ml-3 rounded-md">
            Beli
          </button>
        </div>
        <div class="${btnEdit} mt-4">
        <a href="form-edit-buku.html?id=${data.id}" class="relative bg-zinc-700 hover:bg-zinc-600 text-white p-2 rounded-md cursor-pointer">
        Edit Buku
        </a>
        </div>
      </span>
    </div>
  </div>`
    $('#container-book-detail').append(detailBook)
    const decrementButtons = document.querySelectorAll(
      `button[data-action="decrement"]`
    );
    const incrementButtons = document.querySelectorAll(
      `button[data-action="increment"]`
    );
    decrementButtons.forEach(btn => {
      btn.addEventListener("click", decrement);
    });

    incrementButtons.forEach(btn => {
      btn.addEventListener("click", increment);
    });

  }).catch(error => {
    Swal.fire("Terjadi Kesalahan", error.msg, 'error');
  })
})

function decrement(e) {
  const btn = e.target.parentNode.parentElement.querySelector(
    'button[data-action="decrement"]'
  );
  const target = btn.nextElementSibling;
  let value = Number(target.value);
  if(value > 0){
    value--;
    target.value = value;
  }else {
    Swal.fire("terjadi Kesalahan", "Input angka tidak boleh negatif", "error")
  }

}

function increment(e) {
  const btn = e.target.parentNode.parentElement.querySelector(
    'button[data-action="decrement"]'
  );
  const target = btn.nextElementSibling;
  let value = Number(target.value);
  value++;
  target.value = value;
}

const createOrderAPI= (idBook, quantity) => {
  return new Promise((res, rej) => {
    const formdata = new FormData();
    formdata.append("quantity", quantity);

    const requestOptions = {
      method: 'POST',
      body: formdata,
      headers: {"Authorization": "Bearer " + token}
    };

    fetch(host + `order/create/${idBook}/`, requestOptions)
      .then(async response => {
        if (response.status === 201) {
          const data = await response.json()
          res(data);
        }else if (response.status >= 400 && response.status < 500){
          rej({msg: "terjadi kesalahan dari user"})
        }else{
          rej({msg: "Error pada server"})
        }
      }).catch(error => {
      rej({msg: "Connection error"})
    });
  })
}

createOrder = ()=>{
  createOrderAPI(idBook, document.getElementById("input-quantity").value).then(data => {
    Swal.fire({
      title: 'Berhasil',
      text: "Order anda telah berhasil dibuat",
      icon: 'success',
    }).then((result) => {
      if (result.isConfirmed) {
        location.href ='./dashboard.html'
      }
    })
    }
  ).catch(error =>{
    Swal.fire("Terjadi Kesalahan", error.msg + ", silahkan coba beberapa saat lagi", "error")
  })
}


