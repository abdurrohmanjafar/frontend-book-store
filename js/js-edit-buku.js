var title = document.getElementById("title")
var author = document.getElementById("author")
var publisher = document.getElementById("publisher")
var totalPages = document.getElementById("total-pages")
var isbn = document.getElementById("isbn")
var selectedCategory = document.getElementById("selected-category")
var stock = document.getElementById("stock")
var datePublished = document.getElementById("date-published")
var prices = document.getElementById("prices")
var synopsis = document.getElementById("synopsis")
var btnEditBuku = document.getElementById("btn-edit-buku")

const getDetailBuku = (id) => {
  return new Promise((res, rej) => {
    const requestOptions = {
      method: 'GET'
    };
    fetch(host + `book/${id}/`, requestOptions)
      .then(async response => {
        if (response.status === 200) {
          const data = await response.json()
          res(data);
        } else if (response.status === 404) {
          rej({msg: "Buku tidak ditemukan"})
        } else {
          rej({msg: "Server error"})
        }
      }).catch(error => {
      rej({msg: "Connection error"})
    });
  })
}

const urlParams = new URLSearchParams(location.search)
const idBook = urlParams.get("id")
$(() => {
  force_admin()
  if (urlParams.has('id')) {
    getDetailBuku(idBook).then(data => {
      title.value = data.title
      author.value = data.author
      publisher.value = data.publisher
      totalPages.value = data.pages
      isbn.value = data.isbn
      selectedCategory.value = data.category
      stock.value = data.stocks
      datePublished.value = data.date_published
      synopsis.value = data.synopsis
      prices.value = data.prices
    }).catch(error => {

    })
  }
})

const validateFormData = () => {
  let status = true
  if (totalPages.value < 1 | stock.value < 1 | prices.value < 1) {
    status = false
  }
  return status
}

const EditBuku = () => {
  return new Promise((res, rej) => {
    const formdata = new FormData();
    formdata.append("title", title.value);
    formdata.append("pages", totalPages.value);
    formdata.append("prices", prices.value);
    formdata.append("synopsis", synopsis.value);
    formdata.append("category", selectedCategory.value);
    formdata.append("stocks", stock.value);
    formdata.append("author", author.value);
    formdata.append("date_published", datePublished.value);
    formdata.append("publisher", publisher.value);
    formdata.append("isbn", isbn.value);
    const requestOptions = {
      method: 'POST',
      body: formdata,
      headers: {"Authorization": "Bearer " + token}
    };
    if (validateFormData()) {
      fetch(host + `book/post/${idBook}/`, requestOptions)
        .then(async response => {
          if (response.status === 200) {
            const data = await response.json()
            res(data);
          } else if (response.status >= 500) {
            rej({msg: "Server error"})
          } else {
            const data = await response.json()
            rej({msg: "Input Tidak Valid"})
          }
        }).catch(error => {
        rej({"message": "Connection error"})
      });
    } else {
      Swal.fire("Terjadi kesalahan", "Input tidak valid, mohon dicek kembali", "error")
    }
  })
}
btnEditBuku.onclick = () => {
  EditBuku().then(data => {
    Swal.fire({
      title: 'Berhasil',
      text: "Berhasil mengubah buku",
      icon: 'success',
    }).then((result) => {
      if (result.isConfirmed) {
        location.href = `./detail-buku.html?id=${idBook}`;
      }
    })
  }).catch(error => {
    Swal.fire("Terjadi kesalahan", error.msg, "error")
  })
}
