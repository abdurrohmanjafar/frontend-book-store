var title = document.getElementById("title")
var author = document.getElementById("author")
var publisher = document.getElementById("publisher")
var totalPages = document.getElementById("total-pages")
var isbn = document.getElementById("isbn")
var selectedCategory = document.getElementById("selected-category")
var stock = document.getElementById("stock")
var datePublished = document.getElementById("date-published")
var prices = document.getElementById("prices")
var synopsis = document.getElementById("synopsis")
var btnTambahBuku = document.getElementById("btn-tambah-buku")


$(()=>{
  force_admin()
})

const validateFormData = () => {
  let status = true
  if (totalPages.value < 1 | stock.value < 1 | prices.value < 1) {
    status = false
  }
  return status
}

const TambahBuku = () => {
  return new Promise((res, rej) => {
    const formdata = new FormData();
    formdata.append("title", title.value);
    formdata.append("pages", totalPages.value);
    formdata.append("prices", prices.value);
    formdata.append("synopsis", synopsis.value);
    formdata.append("category", selectedCategory.value);
    formdata.append("stocks", stock.value);
    formdata.append("author", author.value);
    formdata.append("date_published", datePublished.value);
    formdata.append("publisher", publisher.value);
    formdata.append("isbn", isbn.value);
    const requestOptions = {
      method: 'POST',
      body: formdata,
      headers: {"Authorization": "Bearer " + token}
    };
    if (validateFormData()) {
      fetch(host + "book/post/", requestOptions)
        .then(async response => {
          if (response.status === 201) {
            const data = await response.json()
            res(data);
          } else if (response.status >= 500) {
            rej({msg: "Server error"})
          } else {
            const data = await response.json()
            rej({msg: "Input Tidak Valid"})
          }
        }).catch(error => {
        rej({"message": "Connection error"})
      });
    } else {
      Swal.fire("Terjadi kesalahan", "Input tidak valid, mohon dicek kembali", "error")
    }
  })
}
btnTambahBuku.onclick = () => {
  TambahBuku().then(data => {
    Swal.fire({
      title: 'Berhasil',
      text: "Berhasil menambahkan buku",
      icon: 'success',
    }).then((result) => {
      if (result.isConfirmed) {
        location.href = "./buku.html";
      }
    })
  }).catch(error => {
    Swal.fire("Terjadi kesalahan", error.msg, "error")
  })
}
