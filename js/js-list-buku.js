var buttonSearch = document.getElementById('btn-search')
var inputSearch = document.getElementById("input-search")

buttonSearch.onclick = ()=>{

  if(buttonSearch.classList.contains("active-button")){
    inputSearch.classList.remove("w-[300px]")
    inputSearch.classList.add("w-12")
    buttonSearch.classList.remove("active-button")
    buttonSearch.classList.remove("translate-x-[251px]")
    buttonSearch.classList.add("translate-x-[1px]")

  }else{
    inputSearch.classList.remove("w-12")
    inputSearch.classList.add("w-[300px]")
    buttonSearch.classList.add("active-button")
    buttonSearch.classList.remove("translate-x-[1px]")
    buttonSearch.classList.add("translate-x-[251px]")
  }
}

const getAllBuku = () => {
  return new Promise((res, rej) => {
    const requestOptions = {
      method: 'GET'
    };
    fetch(host + "book/", requestOptions)
      .then(async response => {
        if (response.status === 200) {
          const data = await response.json()
          res(data);
        } else {
          rej({msg: "Server error"})
        }
      }).catch(error => {
      rej({msg: "Connection error"})
    });
  })
}

$(()=>{
  if(role === "ADMIN"){
    document.getElementById('btn-edit-buku').classList.remove("hidden")
  }
  getAllBuku().then(data => {
    console.log(data)
    for(let i=0; i < data.length; i++){
      const cardBuku = `<div onclick="clickedBookCard(${data[i].id})" class="mx-2 my-4 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-105 duration-300 card-book w-[250px] p-4 bg-gray-300 rounded-xl drop-shadow-lg border-none cursor-pointer">
    <p class="font-bold w-full truncate">${data[i].title}</p>
    <p class="truncate">${data[i].author}</p>
    <p class="truncate">${data[i].publisher}</p>
    <p class="font-semibold truncate text-green-700">${data[i].prices}</p>
    </div>`
      $('#container-book').append(cardBuku)
    }
  }).catch(error =>{

  })
})

clickedBookCard = (id)=>{
  window.location.href = `./detail-buku.html?id=${id}`
}


dataSearch = document.getElementById('input-search')

const getBuku = (keyword) => {
  return new Promise((res, rej) => {
    const requestOptions = {
      method: 'GET',
    };
    console.log("2")
    fetch(host + `book/find/?keyword=${keyword}`, requestOptions)
      .then(async response => {
        if (response.status === 200) {
          const data = await response.json()
          res(data);
        } else {
          rej({msg: "Server error"})
        }
      }).catch(error => {
        console.log(error)
      rej({msg: "Connection error"})
    });
  })
}

const submitSearch = (e)=>{
  e.preventDefault()
  getBuku(dataSearch.value).then(data =>{
    document.getElementById('container-book').innerHTML = ""
    if (data.length > 0){
      for(let i=0; i < data.length; i++){
        const cardBuku = `<div onclick="clickedBookCard(${data[i].id})" class="mx-2 my-4 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-105 duration-300 card-book w-[250px] p-4 bg-gray-300 rounded-xl drop-shadow-lg border-none cursor-pointer">
    <p class="font-bold w-full truncate">${data[i].title}</p>
    <p class="truncate">${data[i].author}</p>
    <p class="truncate">${data[i].publisher}</p>
    <p class="font-semibold truncate text-green-700">${data[i].prices}</p>
    </div>`
        $('#container-book').append(cardBuku)
      }
    }else{
      const responseEmpty = `<div class="font-bold text-lg">Buku yang anda cari tidak tersedia</div>`
      console.log("sini")
      $('#container-book').append(responseEmpty)
    }

  }).catch(error =>{
      console.log(error)
  })
  console.log(dataSearch.value)
}
