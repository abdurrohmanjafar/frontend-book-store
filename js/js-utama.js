
  var hamburgerBar = document.getElementById("hamburger-bar")
  var hamburgerBarAuth = document.getElementById("hamburger-bar-auth")
  var mobileMenu = document.getElementById("mobile-menu")
  var mobileMenuAuth = document.getElementById("mobile-menu-auth")

  const transformAnimateDown = [
    { transform: 'translateY(0px)' }
  ];

  const transformAnimateUp = [
    { transform: 'translateY(-48rem)' }
  ];

  const transformTiming = {
    duration: 500,
    iterations:1
  }

  const delayInMilliseconds = 500

  if(logged_in){
    hamburgerBar.onclick = function (){
      if (mobileMenu.style.transform == "translateY(0px)"){
        mobileMenu.animate(transformAnimateUp, transformTiming)
        setTimeout(function() {
          mobileMenu.style.transform = "translateY(-48rem)";
        }, delayInMilliseconds);
      }

      else {
        mobileMenu.animate(transformAnimateDown, transformTiming)
        setTimeout(function() {
          mobileMenu.style.transform = "translateY(0px)";
        }, delayInMilliseconds);
      }
    }
  }
  else{
    hamburgerBarAuth.onclick = function (){
      if (mobileMenuAuth.style.transform == "translateY(0px)"){
        mobileMenuAuth.animate(transformAnimateUp, transformTiming)
        setTimeout(function() {
          mobileMenuAuth.style.transform = "translateY(-48rem)";
        }, delayInMilliseconds);
      }

      else {
        mobileMenuAuth.animate(transformAnimateDown, transformTiming)
        setTimeout(function() {
          mobileMenuAuth.style.transform = "translateY(0px)";
        }, delayInMilliseconds);
      }
    }
  }

  window.onclick = ()=> {
    if (mobileMenu.style.transform == "translateY(0px)"){
      mobileMenu.animate(transformAnimateUp, transformTiming)
      setTimeout(()=> {
        mobileMenu.style.transform = "translateY(-48rem)";
      }, delayInMilliseconds);
    }

    if (mobileMenuAuth.style.transform == "translateY(0px)"){
      mobileMenuAuth.animate(transformAnimateUp, transformTiming)
      setTimeout(()=> {
        mobileMenuAuth.style.transform = "translateY(-48rem)";
      }, delayInMilliseconds);
    }
  }
